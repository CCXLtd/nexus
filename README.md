# Docker Container for Nexus


This is conainter for [nexus][1].

## To Build

```
> docker build --tag codered/nexus . # normal build
> docker build --no-cache=true --force-rm=true --tag codered/nexus . # force a full build
```

## To Run

```
>    docker run --name nexus \
           --detach \
           --restart unless-stopped \
           --publish 8081:8081 \
           --volume /var/lib/nexus/sonatype-work:/u01/sonatype-work \
           codered/nexus
 
docker stop nexus
docker start nexus

```

[1]:  http://www.sonatype.com/download-oss-sonatype

